# 数据库建立 命名为 forum，在util类里只需要修改数据库密码
# 将下面命令复制到控制台即可创建数据库或者使用 ctrl+shift+f10

CREATE TABLE comments
(
  id        INT AUTO_INCREMENT
    PRIMARY KEY,
  comment   VARCHAR(1000)      NOT NULL,
  date      VARCHAR(20)        NOT NULL,
  adopt     INT(1) DEFAULT '0' NULL,
  user_id   INT                NOT NULL,
  topic_id  INT                NULL,
  notice_id INT                NULL,
  CONSTRAINT comment_user_id_fk
  FOREIGN KEY (user_id) REFERENCES forum.user (id),
  CONSTRAINT comment_topic_id_fk
  FOREIGN KEY (topic_id) REFERENCES forum.topic (id),
  CONSTRAINT comment_notice_id_fk
  FOREIGN KEY (notice_id) REFERENCES forum.notice (id)
);

CREATE INDEX comment_notice_id_fk
  ON comments (notice_id);

CREATE INDEX comment_topic_id_fk
  ON comments (topic_id);

CREATE INDEX comment_user_id_fk
  ON comments (user_id);

CREATE TABLE notice
(
  id      INT AUTO_INCREMENT
    PRIMARY KEY,
  title   VARCHAR(20)  NOT NULL,
  content VARCHAR(500) NOT NULL,
  user_id INT          NULL,
  date    VARCHAR(50)  NULL,
  CONSTRAINT notice_user_id_fk
  FOREIGN KEY (user_id) REFERENCES forum.user (id)
);

CREATE INDEX notice_user_id_fk
  ON notice (user_id);

CREATE TABLE topic
(
  id      INT AUTO_INCREMENT
    PRIMARY KEY,
  title   VARCHAR(100)           NULL,
  content VARCHAR(500)           NULL,
  points  INT                    NULL,
  user_id INT                    NULL,
  date    VARCHAR(20)            NULL,
  istop   TINYINT(1) DEFAULT '0' NULL,
  CONSTRAINT topic_user_id_fk
  FOREIGN KEY (user_id) REFERENCES forum.user (id)
);

CREATE INDEX topic_user_id_fk
  ON topic (user_id);

CREATE TABLE user
(
  id       INT AUTO_INCREMENT
    PRIMARY KEY,
  username VARCHAR(20)       NOT NULL,
  password VARCHAR(20)       NULL,
  type     VARCHAR(10)       NULL,
  points   INT DEFAULT '100' NULL,
  age      INT               NULL,
  sex      VARCHAR(6)        NULL,
  summary  VARCHAR(100)      NULL
);

