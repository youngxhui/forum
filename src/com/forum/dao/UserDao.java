package com.forum.dao;

import com.forum.bean.User;
import com.forum.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by young on 2017/5/28.
 * 持久层
 */
public class UserDao {
    private Connection connection = JDBCUtil.getConnection();

    public User selectUser(User user) {
        PreparedStatement ps;
        ResultSet rs;
        try {
            String sql = "SELECT * FROM forum.user WHERE id = ?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setAge(rs.getInt("age"));
                user.setSex(rs.getString("sex"));
                user.setPoints(rs.getInt("points"));
                user.setType(rs.getString("type"));
                user.setSummary(rs.getString("summary"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public int UpdateUser(User user) {
        int rs = 0;
        PreparedStatement ps;
        String sql = "UPDATE user SET username=?,password=?,age=?,sex=?,summary=? WHERE id=?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setInt(3, user.getAge());
            ps.setString(4, user.getSex());
            ps.setString(5, user.getSummary());
            ps.setInt(6, user.getId());
            rs = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(rs);
        return rs;
    }

    public int transactionPoint(int formId, int toId, int points) {
        int row = 0;
        String sqlFormUser = "UPDATE user SET points = points-?  WHERE id=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sqlFormUser);
            ps.setInt(1, points);
            ps.setInt(2, formId);
            row = ps.executeUpdate();
            if (row == 1) {
                String sqlToUser = "UPDATE user SET points =points+?  WHERE id=?";
                ps = connection.prepareStatement(sqlToUser);
                ps.setInt(1, points);
                ps.setInt(2, toId);
                ps.executeUpdate();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return row;
    }

    public int InsertUser(User user) {
        int rs = 0;
        PreparedStatement pst = null;
        String sql_insert = "INSERT INTO user(username,password,age,sex,type,summary) VALUES (?,?,?,?,?,?)";
        try {
            pst = connection.prepareStatement(sql_insert);
            pst.setString(1, user.getUsername());
            pst.setString(2, user.getPassword());
            pst.setString(3, String.valueOf(user.getAge()));
            pst.setString(4, user.getSex());
            pst.setString(5, user.getType());
            pst.setString(6, user.getSummary());
            rs = pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    public List<User> CheckUser(User user) {
        List<User> list = new ArrayList<>();
        PreparedStatement pst;
        ResultSet rs;
        System.out.println("user = " + user.getUsername() + " and " + user.getPassword());
        try {
            String sql_check = "SELECT * FROM forum.user WHERE username = ? AND password=?";
            pst = connection.prepareStatement(sql_check);
            pst.setString(1, user.getUsername());
            pst.setString(2, user.getPassword());
            rs = pst.executeQuery();
            if (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setType(rs.getString("type"));
                user.setAge(rs.getInt("age"));
                user.setSex(rs.getString("sex"));
                user.setSummary(rs.getString("summary"));
                user.setPoints(rs.getInt("points"));
                list.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int addPoints(int points, int userId) {
        int flag = 0;
        String sql = "UPDATE user SET points = points+? WHERE user.id=? ;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, points);
            ps.setInt(2, userId);
            flag = ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

}

