package com.forum.dao;

import com.forum.bean.Comment;
import com.forum.bean.User;
import com.forum.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by young on 2017/5/29.
 *
 * @version 1.0
 */
public class CommentDao {

    private Connection connection = JDBCUtil.getConnection();

    /**
     * @param topicId 文章id
     * @return list 为该文章的评论
     * @author 杨晓辉
     */
    public List<Comment> listComment(int topicId) {
        List<Comment> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM comments,user,topic WHERE comments.topic_id=topic.id" +
                    " AND comments.user_id=user.id AND topic.id=? " +
                    "ORDER BY comments.date DESC ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, topicId);
            ResultSet set = ps.executeQuery();
            while (set.next()) {
                Comment comment = new Comment();
                comment.setId(set.getInt("comments.id"));
                comment.setCommentPoints(set.getString("user.points"));
                comment.setComment(set.getString("comment"));
                comment.setDate(set.getString("date"));
                User user = new User();
                user.setId(set.getInt("user_id"));
                user.setUsername(set.getString("username"));
                user.setSex(set.getString("sex"));
                user.setAge(set.getInt("age"));
                comment.setUser(user);
                comment.setAdopt(set.getInt("adopt"));
                list.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * @param topicId 文章id
     * @param userId  用户id
     * @param comment 评论内容
     * @return 1表示成功，0表示失败
     * @author 杨晓辉
     */
    public int addComment(int topicId, int userId, Comment comment) {
        int row = 0;
        String sql = "INSERT INTO comments (comment, date, adopt, user_id, topic_id)" +
                " VALUES (?,?,FALSE,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, comment.getComment());
            ps.setString(2, comment.getDate());
            ps.setInt(3, userId);
            ps.setInt(4, topicId);
            row = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return row;
    }

    /**
     * 该接口是作用于文章采纳
     *
     * @param topicId   文章id
     * @param commentId 评论id
     * @return 1代表成功，0表示失败
     * @author 杨晓辉
     */
    public int updateAdopt(int topicId, int commentId) {
        int flag = 0;
        String sql = "UPDATE comments SET adopt = 1 WHERE id=? ;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, commentId);
            int row = ps.executeUpdate();
            if (row == 1) {
                sql = "UPDATE comments SET adopt = -1 WHERE topic_id=? AND adopt!=1";
                ps = connection.prepareStatement(sql);
                ps.setInt(1, topicId);
                row = ps.executeUpdate();
                if (row == 1) {
                    flag = row;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }
}
