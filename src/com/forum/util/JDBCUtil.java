package com.forum.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by young on 2017/5/28.
 * @version 1.0
 * 示例代码
 * <code>Connection connection=JDBCUtil.getConnection();</code>
 */
public class JDBCUtil {

    private static String DBUsername = "root";
    //请修改为自己数据库密码
    private static String DBPassword = "1234";
    private static String DBURl = "jdbc:mysql://localhost:3306/forum";
    private static String DBDriver = "com.mysql.jdbc.Driver";
    private static Connection connection;

    /**
     * @return connection 获得数据库连接
     */
    public static Connection getConnection() {
        try {
            Class.forName(DBDriver);
            connection = DriverManager.getConnection(DBURl, DBUsername, DBPassword);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
