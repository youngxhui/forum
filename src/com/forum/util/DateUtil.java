package com.forum.util;

import java.util.Calendar;

/**
 * Created by young on 2017/5/30.
 *
 * @version 1.0
 *          主要用于获取当前系统时间
 */
public class DateUtil {
    public static String getDate() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DATE);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        String s;
        String m;
        String h;
        String d;
        String mo;
        if (second < 10) {
            s = "0" + second;
        } else {
            s = String.valueOf(second);
        }
        if (minute < 10) {
            m = "0" + minute;
        } else {
            m = String.valueOf(minute);
        }
        if (hour < 10) {
            h = "0" + hour;
        } else {
            h = String.valueOf(hour);
        }
        String date = year + "/" + month + "/" + day + " " + h + ":" + m + ":" + s;
        return date;
    }
}
