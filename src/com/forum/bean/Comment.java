package com.forum.bean;

/**
 * Created by young on 2017/5/29.
 */
public class Comment {
    private int id;
    private String comment;
    private String date;
    private int adopt;
    private User user;
    private String commentPoints;

    public String getCommentPoints() {
        return commentPoints;
    }

    public void setCommentPoints(String commentPoints) {
        this.commentPoints = commentPoints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getAdopt() {
        return adopt;
    }

    public void setAdopt(int adopt) {
        this.adopt = adopt;
    }
}
