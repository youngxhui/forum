package com.forum.servlet;

import com.forum.bean.User;
import com.forum.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 *
 * @version 1.0
 */
@WebServlet(name = "CheckUserServlet", urlPatterns = "/checkUser")
public class CheckUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        User user = new User();
        user.setUsername(LogonFormServlet.name);
        user.setPassword(LogonFormServlet.pass);
        UserDao userdao = new UserDao();
        List<User> list = userdao.CheckUser(user);
        if (list != null) {
            PrintWriter out = response.getWriter();
            out.println("登录失败");
        }

        for (User u : list
                ) {
            HttpSession session = request.getSession();
            session.setAttribute("id", u.getId());
            session.setAttribute("username", u.getUsername());
            session.setAttribute("password", u.getPassword());
            session.setAttribute("type", u.getType());
            session.setAttribute("points", u.getPoints());
            session.setAttribute("sex", u.getSex());
            session.setAttribute("age", u.getAge());
            session.setAttribute("summary", u.getSummary());
            response.sendRedirect("/index.jsp");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
