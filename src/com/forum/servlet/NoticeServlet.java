package com.forum.servlet;

/**
 * Created by sl on 2017/6/6.
 */

import com.forum.bean.Notice;
import com.forum.dao.NoticeDao;
import com.forum.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "NoticeServlet", urlPatterns = "/addNotice")
public class NoticeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        response.setHeader("content-type", "text/html;charset=UTF-8");
        Notice notice = new Notice();
        notice.setTitle(request.getParameter("title"));
        notice.setContent(request.getParameter("content"));
        notice.setDate(DateUtil.getDate());
        String userId = request.getParameter("userId");
        NoticeDao noticeDao = new NoticeDao();
        int flag = noticeDao.insertNotice(notice, userId);
        if (flag != 1) {
            PrintWriter out = response.getWriter();
            out.println("发布失败");
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}




