package com.forum.servlet;

import com.forum.dao.CommentDao;
import com.forum.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by young on 2017/6/5.
 *
 * @version 1.0
 */
@WebServlet(name = " AdoptServlet", urlPatterns = "/adopt")
public class AdoptServlet extends HttpServlet {
    /**
     * @param request  请求
     * @param response 回复
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int commentId = Integer.parseInt(request.getParameter("commentId"));
        int topicId = Integer.parseInt(request.getParameter("topicId"));
        int userId = Integer.parseInt(request.getParameter("userId"));
        int points = Integer.parseInt(request.getParameter("points"));
        int s = Integer.parseInt(request.getParameter("s"));
        CommentDao commentDao = new CommentDao();
        UserDao userDao = new UserDao();
        int transaction = userDao.transactionPoint(s, userId, points);
        int adopt = commentDao.updateAdopt(topicId, commentId);
        System.out.println("t is " + transaction + " a is " + adopt);
        if (transaction == 1) {
            response.sendRedirect("/article.jsp?topic_id=" + topicId);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
