package com.forum.servlet;

import com.forum.dao.NoticeDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PipedWriter;
import java.io.PrintWriter;

/**
 * Created by Administrator on 2017/6/11.
 */
@WebServlet(name = "DeleteNoticeServlet", urlPatterns = "/deleteNotice")
public class DeleteNoticeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        NoticeDao noticeDao = new NoticeDao();
        int flag = noticeDao.deleteNotice(id);
        PrintWriter out = response.getWriter();
        if (flag != 0) {
            response.sendRedirect("index.jsp");
        } else {
            out.print("失败");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        doPost(request, response);
    }
}

