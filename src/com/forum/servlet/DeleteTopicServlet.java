package com.forum.servlet;

import com.forum.dao.TopicDao;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Administrator on 2017/6/12.
 */
@WebServlet(name = "doDeleteTopicServlet", urlPatterns = "/deleteTopic")
public class DeleteTopicServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        TopicDao  topicDao = new TopicDao();
        int flag = topicDao.deleteTopic(id);
        PrintWriter out = response.getWriter();
        if (flag != 0) {
            response.sendRedirect("index.jsp");
        } else {
            out.print("失败");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        doPost(request, response);
    }
}