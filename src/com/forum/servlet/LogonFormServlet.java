package com.forum.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by Administrator on 2017/6/6.
 */
@WebServlet(name = "LogonFormServlet")
public class LogonFormServlet extends HttpServlet {
    public static String name;
    public static String pass;

    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        name = request.getParameter("name");
        pass = request.getParameter("pass");
        HttpSession session = request.getSession(false);
        if (session == null) {
            out.println(" 验证码处理问题 !");
            return;
        }
        String savedCode = (String) session.getAttribute("check_code");
        if (savedCode == null) {
            out.println(" 验证码处理问题 !");
            return;
        }
        String checkCode = request.getParameter("check_code");
        if (!savedCode.equals(checkCode)) {
                     /* 验证码未通过，不从 Session 中清除原来的验证码，
                     以便用户可以后退回登录页面继续使用原来的验证码进行登录 */
            out.println(" 验证码无效 !");
            return;
        }
              /* 验证码检查通过后，从 Session 中清除原来的验证码，
              以防用户后退回登录页面继续使用原来的验证码进行登录 */
        session.removeAttribute("check_code");
        //out.println(" 验证码通过，服务器正在校验用户名和密码 !");
        response.sendRedirect("/checkUser");
    }

}
