package com.forum.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by young on 2017/5/28.
 *
 * @version 1.0
 */

// 通过注解进行开发，可以不需要再web.xml进行配置，如果要在web.xml进行配置，请把下面这行删除
@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
        out.println("<HTML>");
        out.println("  <HEAD></HEAD>");
        out.println("  <BODY>");
        out.println("<form action='servlet/LogonFormServlet1' method='post'>");
        out.println(" <p>用户名：<input type='text' name='username' value=''><br></p>");
        out.println("<p>密码:<input type='password' name='password' value=''/><br></p>");
        out.println("<p>验证码：<input type='text' name='check_code'>");
        out.println("<img src='servlet/CheckCodeServlet'><br></p>");
        out.println("<input type='submit' value='注册'/>");
        out.println("<input type='reset' value='取消'/>");
        out.println("</form>");
        out.println("  </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}
