package com.forum.servlet;

import com.forum.bean.User;
import com.forum.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by superzhaolu on 2017/6/4.
 */
@WebServlet(name = "SelectServlet", urlPatterns = "/select")
public class SelectServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        request.setCharacterEncoding("utf-8");
        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));
        UserDao dao = new UserDao();
        user = dao.selectUser(user);
        if (user != null) {
            request.setAttribute("user", user);
            request.getRequestDispatcher("SelectUser.jsp").forward(request, response);
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doPost(request, response);
    }
}
