package com.forum.servlet;

import com.forum.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by young on 2017/6/14.
 */
@WebServlet(name = "RechargeServlet", urlPatterns = "/recharge")
public class RechargeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("request.getParameter(id) = " + request.getParameter("id"));
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("r id = " + id);
        int points = Integer.parseInt(request.getParameter("points"));
        UserDao userDao = new UserDao();
        int flag = userDao.addPoints(points, id);
        if (flag != 0) {
            response.sendRedirect("select?id=" + id);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
