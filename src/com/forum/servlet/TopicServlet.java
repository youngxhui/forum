package com.forum.servlet;

import com.forum.bean.Topic;
import com.forum.dao.TopicDao;
import com.forum.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by young on 2017/6/4.
 *
 * @author young
 * @version 1.0
 */
@WebServlet(name = "TopicServlet", urlPatterns = "/addTopic")
public class TopicServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setHeader("content-type", "text/html;charset=UTF-8");
        Topic topic = new Topic();
        topic.setTitle(request.getParameter("title"));
        topic.setContent(request.getParameter("content"));
        topic.setPoints(request.getParameter("points"));
        topic.setDate(DateUtil.getDate());
        String userId = request.getParameter("userId");
        TopicDao topicDao = new TopicDao();
        int flag = topicDao.insertTpoic(topic, userId);
        if (flag != 1) {
            PrintWriter out = response.getWriter();
            out.println("发布失败");
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}
