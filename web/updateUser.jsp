<%@ page import="com.forum.bean.User" %>
<%@ page import="com.forum.dao.UserDao" %><%--
  Created by IntelliJ IDEA.
  User: superzhaolu
  Date: 2017/6/6
  Time: 19:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>updateuser</title>
    <style>
        #table {
            margin: 20px;
        }

        #textarea {
            height: 50px;
            width: 155px;
        }

        .update-img {
            width: 24px;
            height: 24px;
        }
    </style>
</head>
<body>
<jsp:include page="header.jsp"/>
<%
    request.setCharacterEncoding("utf-8");
%>
<form action="updateUser?id=<%=session.getAttribute("id")%>" method="post">
    <table id="table">
        <tr>
            <td><img src="img/Java_96px_1.png" class="update-img">用户名：</td>
            <td><input type="text" name="username" value="<%=session.getAttribute("username")%>"></td>
        </tr>
        <tr>
            <td><img src="img/Money%20Bag_96px.png" class="update-img">密码：</td>
            <td><input type="text" name="password" value="<%=session.getAttribute("password")%>"></td>
        </tr>
        <tr>
            <td><img src="img/Age_96px.png" class="update-img"> 年龄：</td>
            <td><input type="number" name="age" value="<%=session.getAttribute("age")%>"></td>
        </tr>
        <tr>
            <td><img src="img/Gender_96px.png" class="update-img">性别：</td>
            <td><input type="text" name="sex" value="<%=session.getAttribute("sex")%>"></td>
        </tr>
        <tr>
            <td><img src="img/Java_96px_1.png" class="update-img">个人简介：</td>
            <td><textarea name="summary" id="textarea"><%=session.getAttribute("summary")%></textarea></td>
        </tr>
        <tr>
            <td><input type="submit" value="提交"></td>
        </tr>
    </table>

</form>
<jsp:include page="footer.jsp"/>
</body>
</html>
