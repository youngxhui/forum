<%--
  Created by IntelliJ IDEA.
  User: young
  Date: 2017/5/28
  Time: 11:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="shortcut  icon" type="image/x-icon" href="favicon.ico" media="screen"/>
    <title>Java技术论坛</title>

    <link rel="stylesheet" href="css/common.css">
    <style>
        #home-main {
            width: 100%;
            height: 1700px;

        }

        .button-post {
            float: right;
            margin-right: 20px;
        }

        body, head {
            margin: 0;
            padding: 0;
        }

    </style>
</head>
<body>
<div id="home-header">
    <jsp:include page="header.jsp"/>
</div>
<div id="home-main">

    <%
        if (session.getAttribute("id") != null) {
            if (session.getAttribute("type").equals("管理员")) {

    %>
    <a href="addNotice.jsp">
        <button class="button-post">发布公告</button>
    </a>
    <%
            }
        }
    %>
    <h1 style="margin-left: 20px">公告</h1>
    <iframe frameborder="0px" src="notice.jsp" width="100%" height="200px"></iframe>

    <%
        if (session.getAttribute("id") != null) {
    %>
    <a href="addTopic.jsp">
        <button class="button-post">我要发帖</button>
    </a>
    <%
        }
    %>

    <h1 style="margin-left: 20px">帖子</h1>
    <iframe frameborder="0px" src="topicList" width="100%" height="1350px" scrolling="auto"></iframe>


</div>
<div id="home-footer">
    <jsp:include page="footer.jsp"/>
</div>

</body>
</html>
