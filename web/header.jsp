<%--
  Created by IntelliJ IDEA.
  User: young
  Date: 2017/5/28
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <link rel="stylesheet" href="css/common.css">
    <title>hearder</title>
    <style type="text/css">
        #header {
            background-image: url("img/title.png");
            background-size: 1400px 100px;
            width: 100%;
            height: 80px;
            position: relative;
        }

        .header-button {
            float: right;
            margin-top: 20px;
            margin-right: 20px;
        }

        #li-userInfo {
            list-style-type: none;
            margin-right: 20px;
        }

        .ul-userInfo {
            float: right;
        }

        a {
            text-decoration: none;
        }

        .header-a:visited {
            color: white;
        }

        .header-a:link {
            color: white;
        }

        #h1-title {
            float: left;
            width: 20%;
            margin-top: 10px;
            margin-left: 20px;
        }


    </style>
</head>
<body>
<div id="header">
    <h1 id="h1-title" style="margin-left: 20px;padding-left: 20px"><a class="header-a" href="index.jsp">Java 论坛</a>
    </h1>
    <%
        if (session.getAttribute("id") != null) {
    %>
    <li id="li-userInfo">
        <ul class="ul-userInfo">
            <a href="logout" class="header-a">注销
            </a>
        </ul>
        <ul class="ul-userInfo">
            <a class="header-a" href="select?id=<%=session.getAttribute("id")%>"><%=session.getAttribute("username")%>
            </a>
        </ul>
    </li>
    <%
    } else {
    %>
    <a href="login.jsp">
        <button id="button-login" class="header-button">登录</button>
    </a>
    <a href="agreement.jsp">
        <button id="button-register" class="header-button">注册</button>
    </a>
    <%
        }
    %>
</div>
</body>
</html>