<%@ page import="com.forum.bean.Notice" %>
<%@ page import="com.forum.dao.NoticeDao" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: young
  Date: 2017/6/12
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta charset="UTF-8">
    <style>
        #main {
            width: 80%;
            height: 500px;
            margin-left: 20px;
        }

        #p-notice {

            font-size: 40px;
        }
    </style>
</head>
<body>
<jsp:include page="header.jsp"/>
<div id="main">

    <p id="p-notice">公告</p>

    <%
        request.setCharacterEncoding("utf-8");
        response.setHeader("content-type", "text/html;charset=UTF-8");
        int id = Integer.parseInt(request.getParameter("notice_id"));
        String content;
        NoticeDao noticeDao = new NoticeDao();
        List<Notice> list = noticeDao.listNotice(id);
        for (Notice n : list
                ) {
            content = n.getContent();%>

    <table>
        <tr>
            <td>
                <img width="46px" height="46px" src="img/Urgent%20Message_96px.png">
            </td>
            <td style="padding-left: 30px">
                <%=content%>
            </td>
        </tr>
    </table>
    <%
        }
    %>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
