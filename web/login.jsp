<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/6/6
  Time: 11:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>

    <link rel="stylesheet" href="css/common.css">
    <style>
        .td-img {
            width: 24px;
            height: 24px;
        }

        #div-login {
            margin-left: 40%;
            margin-top: 20px;
            height: 400px;
        }
    </style>
    <SCRIPT>
        function check(login) {
            var username = login.name.value;
            var password = login.pass.value;
            if (username === "") {
                alert("账户名不能为空");
                return false;
            } else if (password === "") {
                alert("密码不能为空");
                return false;
            } else {
                return true;
            }
        }
    </SCRIPT>
</head>
<body>
<jsp:include page="header.jsp"/>

<div id="div-login">
    <form action="servlet/LogonFormServlet" method="post" name="login" onsubmit="return check(this)">
        <table cellspacing="20">
            <tr>
                <td><img src="img/Java_96px_1.png" class="td-img"></td>
                <td>用户名：</td>
                <td><input type="text" name="name"></td>
            </tr>
            <tr>
                <td><img class="td-img" src="img/Password_96px.png"></td>
                <td>密 码：</td>
                <td><input type="password" name="pass"></td>
            </tr>
            <tr>
                <td><img class="td-img" src="img/Fingerprint%20Scan_96px.png"></td>
                <td>验证码：</td>
                <td><input type="text" name="check_code"> &nbsp;
                    <img src="servlet/CheckCodeServlet"></td>
            </tr>
            <tr align="center">
                <td colspan="3">
                    <button type="submit">登录</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="reset">取消</button>
                </td>
            </tr>
        </table>
    </form>
</div>

<jsp:include page="footer.jsp"/>

</body>
</html>
