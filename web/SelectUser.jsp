<%@ page import="com.forum.bean.Topic" %>
<%@ page import="com.forum.bean.User" %>
<%@ page import="com.forum.dao.TopicDao" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: superzhaolu
  Date: 2017/6/4
  Time: 17:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SelectUser</title>
    <style>
        #table {
            margin-left: 20px;
            margin-top: 20px;
            margin-bottom: 15px;
        }

        .img-select {
            width: 24px;
            height: 24px;
        }

        #topic-table {
            width: 100%;
        }
    </style>
</head>
<body>
<%
    request.setCharacterEncoding("utf-8");
    User user = (User) request.getAttribute("user");
%>
<jsp:include page="header.jsp"/>
<table id="table">
    <tr>
        <td>
            <img src="img/Java_96px_1.png" class="img-select">
            姓名:<%=user.getUsername()%>
        </td>
        <td>
            <img src="img/Money%20Bag_96px.png" class="img-select">
            积分：<%=user.getPoints()%>
        </td>
        <%
            if (session.getAttribute("id") != null) {
                if (session.getAttribute("username").equals(user.getUsername())) {

        %>
        <td style="padding: 50px">
            <a href="money.jsp?id=<%=user.getId()%>">
                <button>积分管理</button>
            </a>
        </td>
        <%
                }
            }
        %>
    </tr>
    <tr>
        <td>
            <img src="img/Age_96px.png" class="img-select">
            年龄：<%=user.getAge()%>
        </td>
    </tr>
    <tr>
        <td><img src="img/Gender_96px.png" class="img-select">
            性别：<%=user.getSex()%>
        </td>
    </tr>
    <tr>
        <td>
            <img src="img/Note_96px.png" class="img-select">
            简介：<%=user.getSummary()%>
        </td>
    </tr>
    <%
        if (session.getAttribute("id") != null) {
            if (session.getAttribute("username").equals(user.getUsername())) {

    %>
    <tr>
        <td>
            <a href="updateUser.jsp?id=<%=session.getAttribute("id")%>">
                <button>修改信息</button>
            </a>
        </td>
    </tr>
    <%
            }
        }
    %>
</table>

<%
    TopicDao topicDao = new TopicDao();
    List<Topic> list = topicDao.userTopic(user.getId());
%>
<table cellspacing="20" id="topic-table">
    <tr>
        <td width="40%">问题</td>
        <td width="30%">分数</td>
        <td width="20%">时间</td>
        <td width="20%">管理</td>
    </tr>
    <%
        for (Topic t : list
                ) {
    %>
    <tr>
        <td><a href="article.jsp?topic_id=<%=t.getTopicId()%>" target="_blank"><%=t.getTitle()%>
        </a>
        </td>
        <td><%=t.getPoints()%>
        </td>
        <td><%=t.getDate()%>
        </td>

        <td>
            <%
                if (session.getAttribute("id") != null) {
                    if (session.getAttribute("username").equals(user.getUsername()) ||
                            session.getAttribute("type").equals("管理员")) {

            %>

            <a href="deleteTopic?id=<%=t.getTopicId()%>">删除</a>
            <%
                    }
                }

            %>
        </td>
    </tr>
    <%
        }

    %>
</table>


<jsp:include page="footer.jsp"/>
</body>
</html>
