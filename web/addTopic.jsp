<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    int points = (int) session.getAttribute("points");
    System.out.println("points = " + points);
%>
<html>
<head>
    <title>添加文章</title>
    <link rel="stylesheet" href="css/common.css">
    <style>
        #addTopic-main {
            width: 90%;
            height: 500px;
            margin: 20px auto 0;
        }

        #addTopic-input {
            width: 100%;
            height: 40px;
            font-size: 35px;
        }

        #editor {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }

        #button-addTopic {
            float: right;
            margin-right: 20px;
            margin-top: 20px;
            margin-left: 20px;
        }
    </style>

    <script>
        function isEmpty(add) {
            var title = add.title.value;
            var content = add.content.value;
            var points = add.points.value;
            if (title === "" || content === "") {
                alert("标题和内容不能非空");
                return false;
            } else if (points >=<%=points%>) {
                alert("您的积分不够");
                return false;
            } else {
                return true;
            }
//            js这是什么破语法
        }
    </script>

</head>

<body>
<jsp:include page="header.jsp"/>

<div id="addTopic-main">
    <form action="addTopic?userId=<%=session.getAttribute("id")%>" method="post" name="add"
          onsubmit="return isEmpty(this)">
        <input name="title" type="text" placeholder="文章标题" id="addTopic-input">
        <br>
        <textarea id="editor" name="content"></textarea>
        <input name="points" type="number" placeholder="积分" style="margin-top: 20px">
        <button id="button-addTopic" type="submit" name="submit">提交</button>
        <br>
    </form>
</div>

<div style="margin-top: 20px">
    <jsp:include page="footer.jsp"/>
</div>
</body>
</html>
