<%@ page import="com.forum.bean.Topic" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Topic</title>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/topic.css">
</head>
<body>
<div id="main">
    <%

        List<Topic> list = (List<Topic>) request.getAttribute("list");
    %>
    <hr style="margin-left: 20px">

    <%
        for (Topic topic : list
                ) {%>
    <div>
        <center>
            <table cellspacing="20px" id="table-tr">
                <tr>
                    <td width="80%" valign="middle" COLSPAN="2" id="td-title">
                        <a class="topic-a" href="article.jsp?topic_id=<%=topic.getTopicId()%>"
                           target="_blank">
                            <%
                                if (topic.getIsTop() >= 5) {
                            %>
                            <img src="img/Fire%20Element_96px.png" id="pic-fire">
                            <%
                                }
                            %>
                            <%=topic.getTitle()%>
                        </a>
                    </td>
                    <td>
                    </td>
                    <td>
                        <%
                            if (session.getAttribute("id") != null) {
                                if (session.getAttribute("username").equals(topic.getUser().getUsername()) ||
                                        session.getAttribute("type").equals("管理员")) {

                        %>
                        <a class="p-re" href="deleteTopic?id=<%=topic.getTopicId()%>" target="_parent">删除</a>
                        <%
                                }
                            }
                        %>
                    </td>
                </tr>

                <tr>
                    <td width="50%" valign="middle">
                        <a class="topic-a"
                           href="select?id=<%=topic.getUser().getId()%>"
                           target="_parent"><%=topic.getUser().getUsername()%>
                        </a>
                    </td>
                    <td width="10%" valign="middle" id="td-points"><%=topic.getPoints()%>
                    </td>
                    <td id="td-date" width="20%" valign="middle">
                        <%=topic.getDate()%>
                    </td>
                    <td>
                        <%
                            if (session.getAttribute("id") != null) {
                                if (session.getAttribute("type").equals("管理员")) {

                        %>
                        <a class="p-re" href="setTop?id=<%=topic.getTopicId()%>" target="_parent"> 置顶</a>
                        <%
                                }
                            }
                        %>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    <%
        } %>

    <div id="page">
        <hr style="margin-left: 20px">
        <center>
            <%=request.getAttribute("bar")%>
        </center>
    </div>
</div>
</body>
</html>
