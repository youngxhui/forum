<%@ page import="com.forum.dao.NoticeDao" %>
<%@ page import="com.forum.bean.Notice" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: young
  Date: 2017/5/28
  Time: 17:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Topic</title>
    <link rel="stylesheet" href="css/common.css">
    <style>
        .notice-a:link {
            color: #5c5dbe;
        }

        .notice-a:visited {
            color: #5c5dbe;
        }

        .notice-a {
            text-decoration-line: none;
        }
    </style>
</head>
<body>
<%
    NoticeDao noticeDao = new NoticeDao();
    List<Notice> list = noticeDao.listNotice();
%>
<hr style="margin-left: 20px">
<table border="0px" style="margin-left: 20px">
    <tr>
        <td width="40%">内容</td>
        <td width="20%" align="right">发布时间</td>
    </tr>


    <%
        for (Notice notice : list
                ) {%>

    <tr>
        <td width="60%">
            <a class="notice-a" href="showNotice.jsp?notice_id=<%=notice.getId()%>"
               target="_blank"><%=notice.getTitle()%>
            </a>
        </td>

        <td width="20%" align="right">
            <%=notice.getDate()%>
        </td>
        <%
            if (session.getAttribute("id") != null) {
                if (session.getAttribute("type").equals("管理员")) {

        %>
        <td width="10%" align="center">

            <a href="deleteNotice?id=<%=notice.getId()%>"
               target="_parent">删除公告</a>
        </td>
        <%
                }
            }
        %>
    </tr>

    <%
        } %>
</table>
<hr style="margin-left: 20px">
</body>
</html>
