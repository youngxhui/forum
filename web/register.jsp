<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/6/6
  Time: 10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册</title>
    <link rel="stylesheet" href="css/common.css">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <style>
        .img-register {
            width: 24px;
            height: 24px;
        }

        #div-register {
            margin-left: 40%;
            margin-top: 20px;
            height: 400px;
        }
    </style>

    <script type="text/javascript">
        function check(register) {
            var username = register.username.value;
            var password = register.password.value;
            var age = register.age.value;
            var sex = register.sex.value;
            if (username === "") {
                alert("用户名不能为空！");
                register.username.focus();
                register.username.select();
                return false;
            }
            else if (password === "") {
                alert("密码不能为空！");
                register.password.focus();
                register.password.select();
                return false;
            }
            else if (password.length < 5) {
                alert("密码不能少于6位！");
                register.password.focus();
                register.password.select();
                return false;
            } else if (age === "" || age <= 0) {
                alert("年龄不合法");
                register.age.focus();
                register.age.select();
                return false;
            } else if (sex === "") {
                alert("请选择性别！");
                register.sex.focus();
                register.sex.select();
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
<jsp:include page="header.jsp"/>

<div id="div-register">
    <form action="/servlet/InsertUserServlet" method="post" name="register" onsubmit="return check(this)">
        <table cellspacing="20">
            <tr>
                <td><img class="img-register" src="img/Java_96px_1.png"/></td>
                <td>用户名:</td>
                <td><input type="text" name="username" maxlength="20"/></td>
            </tr>
            <tr>
                <td><img class="img-register" src="img/Password_96px.png"/></td>
                <td>密 码:</td>
                <td><input type="password" name="password" maxlength="20"/></td>
            </tr>
            <tr>
                <td><img class="img-register" src="img/Age_96px.png"/></td>
                <td>年 龄:</td>
                <td><input type="number" name="age" maxlength="20"></td>
            </tr>
            <tr>
                <td><img class="img-register" src="img/Gender_96px.png"/></td>
                <td>性 别:</td>
                <td><input type="radio" name="sex" value="男">男&nbsp;
                    <input type="radio" name="sex" value="女">女
                </td>
            </tr>
            <tr>
                <td><img class="img-register" src="img/Note_96px.png"/></td>
                <td>个人介绍:</td>
                <td><textarea cols="30" rows="6" name="summary"></textarea></td>
            </tr>
            <tr align="center">
                <td colspan="3">
                    <button type="submit">注册</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="reset">取消</button>
                </td>
            </tr>
        </table>
    </form>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
