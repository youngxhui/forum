<%@ page import="com.forum.bean.Comment" %>
<%@ page import="com.forum.bean.Topic" %>
<%@ page import="com.forum.dao.CommentDao" %>
<%@ page import="com.forum.dao.TopicDao" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%
    int topicId = Integer.parseInt(request.getParameter("topic_id"));

    TopicDao topicDao = new TopicDao();
    List<Topic> topicList = topicDao.listTopic(topicId);
    String date = null;
    String content = null;
    String title = null;
    String point = null;
    String userName = null;
    String userSex = null;
    int userAge = 0;
    int userPoints = 0;
    int flag = 0;
    for (Topic t : topicList
            ) {
        content = t.getContent();
        date = t.getDate();
        title = t.getTitle();
        point = t.getPoints();
        userName = t.getUser().getUsername();
        userAge = t.getUser().getAge();
        userSex = t.getUser().getSex();
        userPoints = t.getUser().getPoints();
    }

%>
<head>
    <title><%=title%>
    </title>
    <link rel="stylesheet" href="css/common.css">
    <style>

        #comment-button {
            margin-left: 20px;
        }

        #button-adopt {
            background-color: greenyellow;
            color: white;
        }

        #button-adopt:hover {
            background-color: greenyellow;
            color: white;
        }

        .article-button {
            margin-top: 20px;
            margin-right: 20px;
        }

        #topic-article {
            width: 100%;
            height: auto;
        }

        .right-info {
            float: left;
            width: 20%;
        }

        .left-content {
            width: 78%;
            float: right;
            margin-right: 20px;
            margin-top: 20px;
        }

        .img-article {
            width: 24px;
            height: 24px;
        }

        #title-article {
            width: 100%;
            height: 40px;
        }

        .comment {
            margin-top: 20px;
            width: 100%;
            height: 200px;
        }

        #topic {
            margin-top: 10px;
            width: 100%;
            height: 140px;
        }

        #footer {
            width: 100%;
        }

        #article-comment {
            width: 100%;
            height: 300px;
        }
    </style>
</head>
<body>
<jsp:include page="header.jsp"/>
<div id="topic-article">
    <div id="title-article" class="article-div">
        <table width="98%" style="margin-top: 5px">
            <tr style="width: 100%;">
                <td style="margin:20px"><h2>[问题]<%=title%>
                </h2></td>
                <td style="width: 30%;float: right"><h3>积分:<%=point%>
                </h3></td>
            </tr>
        </table>

    </div>
    <div id="topic">
        <div id="right-article" class="right-info">
            <table style="margin-top: 20px;margin-left: 20px">
                <tr>
                    <td><img src="img/Java_96px_1.png" class="img-article"></td>
                    <td>姓名：</td>
                    <td><%=userName%>
                    </td>
                </tr>
                <tr>
                    <td><img src="img/Age_96px.png" class="img-article"></td>
                    <td>年龄：</td>
                    <td><%=userAge%>
                    </td>
                </tr>
                <tr>
                    <td><img src="img/Gender_96px.png" class="img-article"></td>
                    <td>性别：</td>
                    <td><%=userSex%>
                    </td>
                </tr>
                <tr>
                    <td><img src="img/Money%20Bag_96px.png" class="img-article"></td>
                    <td>积分：</td>
                    <td><%=userPoints%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="left-article" class="left-content">
            <table width="100%">
                <tr>
                    <td>
                        <iframe style="border: solid" frameborder="0" src="article/content.jsp?content=<%=content%>"
                                width="100%" height="120px"></iframe>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <%=date%>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br>
    <h2 style="margin-left: 20px">回答</h2>
    <%
        CommentDao commentDao = new CommentDao();
        List<Comment> commentList = commentDao.listComment(topicId);
        String comment;
        for (Comment comments : commentList
                ) {
            comment = comments.getComment();
    %>
    <div class="comment">
        <div id="comment-right" class="right-info">
            <table style="margin-left: 20px;margin-top: 30px" align="center">
                <tr>
                    <td><img src="img/Java_96px_1.png" class="img-article"></td>
                    <td>姓名：</td>
                    <td><%=comments.getUser().getUsername()%>
                    </td>
                </tr>
                <tr>
                    <td><img src="img/Age_96px.png" class="img-article"></td>
                    <td>年龄：</td>
                    <td><%=comments.getUser().getAge()%>
                    </td>
                </tr>
                <tr>
                    <td><img src="img/Gender_96px.png" class="img-article"></td>
                    <td>性别：</td>
                    <td><%=comments.getUser().getSex()%>
                    </td>
                </tr>
                <tr>
                    <td><img src="img/Money%20Bag_96px.png" class="img-article"></td>
                    <td>积分：</td>
                    <td><%=comments.getCommentPoints()%>
                    </td>
                </tr>
            </table>
        </div>

        <div id="left-comment" class="left-content">
            <table width="100%">
                <tr>
                    <td>
                        <iframe style="border: solid"
                                src="article/comment.jsp?comment=<%=comment%>" width="100%" height="120px">
                        </iframe>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <%=comments.getDate()%>
                        <%
                            if (session.getAttribute("id") != null) {
                                if (session.getAttribute("username").equals(userName)) {
                                    if (comments.getAdopt() == 0) {
                        %>
                        <a href="adopt?commentId=<%=comments.getId()%>&topicId=<%=topicId%>&userId=<%=comments.getUser().getId()%>&s=<%=session.getAttribute("id")%>&points=<%=point%>">
                            <button class="article-button">采纳</button>
                        </a>
                        <%
                        } else if (comments.getAdopt() == 1) {
                            flag = 1;
                        %>
                        <button id="button-adopt" class="article-button">已采纳</button>
                        <%
                                    }
                                }
                            }
                        %>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%
        }
    %>
    <br>
</div>
<%
    if (session.getAttribute("id") != null) {
        if (flag != 1) {
%>
<div id="article-comment">
    <h2 style="margin-left: 20px">评论</h2>
    <form action="addComment?topic_id=<%=topicId%>&user_id=<%=session.getAttribute("id")%>"
          method="post">
        <textarea name="comment" style="margin: 20px ;width: 90%;" rows="10"></textarea>
        <br>
        <button id="comment-button" type="submit">确定</button>
    </form>
</div>
<%
        }
    }
%>
<div id="footer">
    <jsp:include page="footer.jsp"/>
</div>
</body>
</html>