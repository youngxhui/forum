<%--
  Created by IntelliJ IDEA.
  User: young
  Date: 2017/5/28
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>服务条款</title>

    <link rel="stylesheet" href="css/common.css">
    <style>
        #button-submit {
            margin-top: 20px;
            margin-right: 20px;
            left: 48%;
        }

        #agreement-table {
            margin-left: 70%;
        }
    </style>
</head>
<body>
<jsp:include page="header.jsp"/>
<center>
    <H1>服务条款</H1>

    <iframe width="70%" height="400px" src="content.jsp">
    </iframe>

</center>

<a href="/register.jsp">
    <button id="button-submit">同意</button>
</a>

<jsp:include page="footer.jsp"/>
</body>
</html>
