<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/6/6
  Time: 19:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>发布公告</title>
    <link rel="stylesheet" href="css/common.css">
    <style>
        #addNotice-main {
            width: 90%;
            height: 500px;
            margin: 20px auto 0;
        }

        #addNotice-input {
            width: 100%;
            height: 40px;
            font-size: 35px;
        }

        #editor {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }

        #button-addNotice {
            float: right;
            margin-right: 20px;
            margin-top: 20px;
            margin-left: 20px;
        }
    </style>

    <script>
        function isEmpty(add) {
            var title = add.title.value;
            var content = add.content.value;
            if (title === "" || content === "") {
                alert("标题和内容不能非空");
                return false;

            } else {
                return true;}

        }
    </script>

</head>

<body>
<jsp:include page="header.jsp"/>

<div id="addNotice-main">
    <form action="/addNotice?userId=<%=session.getAttribute("id")%>" method="post" name="add"
          onsubmit="return isEmpty(this)">
        <input name="title" type="text" placeholder="公告标题" id="addNotice-input">
        <br>
        <textarea id="editor" name="content"></textarea>


        <button id="button-addNotice" type="submit" name="submit">发布</button>
        <br>
    </form>
</div>
<div style="margin-top: 20px">
    <jsp:include page="footer.jsp"/>
</div>
</body>
</html>