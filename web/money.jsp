<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>充值</title>
    <style>
        #main {
            width: 90%;
            height: 60%;
            margin-top: 20px;
            margin-left: 20px;
        }

    </style>
</head>

<body>
<jsp:include page="header.jsp"/>
<%
    int id = Integer.parseInt(request.getParameter("id"));
%>
<div id="main">
    <center>
        <form action="recharge?id=<%=id%>" method="post">
            <table>
                <tr>
                    <td><input type="number" name="points" placeholder="您要充值多少积分"></td>
                    <td>
                        <a>
                            <button type="submit">充值</button>
                        </a>
                    </td>
                </tr>
            </table>
        </form>
    </center>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>