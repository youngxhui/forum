<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>footer</title>
    <link rel="stylesheet" href="css/common.css">
    <style>
        #footer {
            width: 100%;
            background-image: url("/img/footer.png");
            background-size: 1350px;
            bottom: 0;
        }

        .td-name {
            text-align: center;
        }


    </style>
</head>
<body>

<DIV id="footer" align="center">
    <hr>
    <font color="white"><p id="p-footer">© Java 论坛</p></font>
    <p><a href="mailto:youngxhui@163.com" id="email">联系我们</a></p>
    <table align="center" border="0px">

        <tr class="td-name"><font color="#f0f8ff">关于我们</font></tr>
        <tr>
            <td class="td-name"><font color="#f0f8ff">张丹</font></td>
            <td class="td-name"><font color="#f0f8ff">石磊</font></td>
        </tr>
        <tr>
            <td class="td-name"><font color="#f0f8ff">杨晓辉</font></td>
            <td class="td-name"><font color="#f0f8ff">李昭璐</font></td>
        </tr>
    </table>
</DIV>
</body>
</html>